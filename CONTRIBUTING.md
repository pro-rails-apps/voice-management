CONTRIBUTING
============

We appreciate all contributions! If you would like to contribute, please follow these steps:

* Fork the repo.
* Create a branch with a name that describes the change.
* Make your changes in the branch.
* Submit a pull-request to merge your feature-branch in our master branch.