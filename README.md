Voice Management
================

[![build status](https://gitlab.com/pro-rails-apps/voice-management/badges/master/build.svg)](https://gitlab.com/pro-rails-apps/voice-management/commits/master)

[This software uses [Semantic Versioning](http://semver.org/) (semver)]

Voice Management provides a simple structure for keeping track of the finance and stay of the devotees at the VOICE.

Any suggestions are welcome and contributions appreciated.

Contributing
------------

We appreciate all contributions! If you would like to contribute, please follow these steps:

* Fork the repo.
* Create a branch with a name that describes the change.
* Make your changes in the branch.
* Submit a pull-request to merge your feature-branch in our master branch.


License
-------

Voice Management is licensed under MIT License